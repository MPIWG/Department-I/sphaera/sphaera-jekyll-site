FROM jekyll/builder:4.0
ADD . /srv/jekyll
COPY --chown=jekyll:jekyll Gemfile .
COPY --chown=jekyll:jekyll Gemfile.lock .
RUN bundle update listen
RUN bundle update
RUN bundle install

