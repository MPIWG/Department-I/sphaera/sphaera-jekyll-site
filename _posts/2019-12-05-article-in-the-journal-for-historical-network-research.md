---
layout: post
title: Article in the Journal for Historical Network Research
---
We have published an article in the Journal of Historical Network research.

Read the article [here](https://jhnr.uni.lu/index.php/jhnr/article/view/63)!

**Abstract**

![muxVizVisualisation](/Department-I/sphaera/sphaera-jekyll-site/assets/img/article_63_cover_en_us.jpg "Visualization of the multi-layer network by means of muxViz ")
*Visualization of the multi-layer network by means of muxViz*

The present work investigates the process of emergence of new epistemic communities. The research is based on semantic, content-related data extracted from a corpus of 359 printed editions, mainly of textbooks used to teach cosmology at European universities between 1472 and 1650. Epistemic communities are identified as families of editions, grouped according to their content, that eventually came to shape knowledge within and by way of the European educational framework. First, a method of classifying the textual content of the books is introduced. Second, a directed, multiplex network is constructed in five layers whose structures are defined specifically for the research question at hand. Then the network is analyzed, first by making use of the aggregated graph—which accounts for the connectivity between books when any of the potential semantic relations are indistinctly considered—and second by showing the contribution of each layer to the emergence of new families of editions. Finally, we interpret the results within a historical framework and identify an epistemic community that represents continuity with the medieval tradition, plus two new scientific and diverging communities that originated in the cultural context of the Reformed countries, which appear in the 1530s. The characteristics of the identified epistemic communities are further analyzed in order to draw general inferences concerning mechanisms of emergence of epistemic communities and their identification in corpora of historical sources. The work concludes by describing future research endeavors related to the corpus, also based on new series of data.
