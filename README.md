# Sphaera Jekyll Site

The Sphaera Microsite based on the MPIWG Jekyll theme.


## How to use

- Add <filename>.md files to /pages folder for a new page.
- Add <personname>.md to the /_people folder for a new peerson eentry.

### Jekyll Install

If you want to [install jekyll](https://jekyllrb.com/docs/installation/).

To install the dependences using [Bundler](https://bundler.io/) run:
```
bundle install
```

Then
```
jekyll serve
```

or

```
bundle exec jekyll serve
```


### Use Docker

Run:

```
docker run --rm \
  --volume="$PWD:/srv/jekyll" \
  --publish 4000:4000 \
  -it jekyll/builder jekyll serve
```

With Mac M1 run:

```
docker run --rm \
  --volume="$PWD:/srv/jekyll" \
  --publish 4000:4000 \
  --platform linux/amd64 \
  -it jekyll/builder jekyll serve
```

### Netlify

To administrate the site using [Netlify CMS](https://www.netlifycms.org/) deploy to server or run locally with https:

```
jekyll serve --ssl-cert _ssl/server.crt --ssl-key _ssl/server.key 
```

Then access https://localhost:4000/admin
