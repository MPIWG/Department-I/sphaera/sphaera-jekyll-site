---
---


var index = elasticlunr(function () {
  this.addField('title')
  this.addField('authors')
  this.addField('languages')
  this.addField('link')
  this.addField('year')
  this.setRef('id')
});

  checkboxes = document.getElementsByName("checkbox");
  

{% assign count = 0 %}{% for book in site.data.books %}

index.addDoc({
  title: {{ book['label'] | jsonify}},
  authors: {{ book['authors'] |replace: ";", ". " |jsonify}},
  languages: {{ book['languages'] | jsonify }} ,
  "link": {{ book['link'] | jsonify}},
  "year": {{ book['year_published_to'] | jsonify}},
  id: {{count}}
});
{% assign count = count | plus: 1 %}{% endfor %}


var store = [{% for book in site.data.books %}{
  "title": {{ book['label'] | jsonify}},
  "authors": {{ book['authors']| replace: ";", ". " | jsonify}},
  "link": {{ book['subject'] | jsonify}},
  "languages": {{ book['languages'] | jsonify}},
  "year": {{ book['year_published_to'] | jsonify}},
}
{% unless forloop.last %},{% endunless %}{% endfor %}]


function doSearch() {
  var resultdiv = $('#results');
  var query = $('input#search').val();
  
  var options = {
    fields: {
      title:{boost: 1},
      authors:{boost: 1},
      languages:{boost: 1}
    },
    bool: "OR",
    expand: true
  };
  
  $('input[type=checkbox]').each(function () {
      if(!this.checked){
        delete options.fields[this.id]
      }
  });
  var result = index.search(query, options);

  

  resultdiv.empty();
  if (result.length == 0) {
    resultdiv.append('<p class="">No results found.</p>');
  } else if (result.length == 1) {
    resultdiv.append('<p class="">Found '+result.length+' result</p>');
  } else {
    resultdiv.append('<p class="">Found '+result.length+' results</p>');
  }

  var firstRun = false;
  var searchitems = "";
  for (var item in result){
    if(!firstRun){
    resultdiv.append( "<table>");
    firstRun = true;
    }
    var ref = result[item].ref;
    searchitems += '<tr><td><a href="' + store[ref].link + '">  --> </a></td><td>' + store[ref].title  + '</td><td>' + store[ref].authors + '</td><td>' + store[ref].languages + ', ' +  store[ref].year + '</td></tr>';
  }
  resultdiv.append(searchitems);

  if(firstRun){
  resultdiv.append('</table>');
  }
}

$(document).ready(function() {
  if (qd.q) {
    $('input#search').val(qd.q[0]);
    doSearch();
  }
  $('input#search').on('keyup', doSearch);
});


var qd = {};
location.search.substr(1).split("&").forEach(function(item) {
    var s = item.split("="),
        k = s[0],
        v = s[1] && decodeURIComponent(s[1]);
    (k in qd) ? qd[k].push(v) : qd[k] = [v]
});
