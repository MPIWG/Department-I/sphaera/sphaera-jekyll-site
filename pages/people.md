---
layout: people
title: People
date: 2019-11-25T21:04:42.268Z
titleimage: /Department-I/sphaera/sphaera-jekyll-site/assets/img/people_banner.png
permalink: /people/
---

The team members of the project _The Sphere_ are engaged in three distinct areas of research and are affiliated with various institutions across multiple countries.

The first area concerns the historical research; the second, the development of Digital Humanities tools; the third the acquisition of the historical sources, of which the corpus is constituted; the fourth the presentation and the digital output of the project, and the fifth support concerning the production and cleaning of metadata as well as bibliographic research and acquisition of necessary secondary literature.
