---
layout: default
title: Theorica planetarum
date: 2019-11-25T22:25:34.047Z
titleimage: /assets/img/Theorica.jpg
permalink: /collections/theorica
---

{% intro The <i>Theorica</i>: From Mathematical Astronomy to the “Real Structure and Motions of the Spheres” %}

The *Theorica planetarum* constitutes a well-established literary genre in medieval Latin culture, which was generally the basis for the teaching of higher astronomy. During the thirteenth to fifteenth centuries, many authors applied themselves to this literary genre. Each in their own way, scholars such as Jean de Lignières, Gerard of Cremona, Campanus of Novara, Walther Brytte, and Johannes de Fundis composed an original *Theorica* to present the mathematical systems of the *Almagest* suitably simplified.
Georg von Peurbach is also embedded in this tradition, but distanced himself from his predecessors by producing in 1454 his own “*Theorica nova*”. This was absolutely “new,” since it “describes the real structure and motions of the spheres” (*Theorica nova realem sperarum habitudinem atque motum cum terminis tabularum declarans*), considering both the spheres of the planets as well as the sphere of the fixed stars.
His text provides a vision of the universe that is truly innovative. This deep innovation makes it possible to affirm that one century before Copernicus made his astronomical revolution, Georg von Peurbach had already made his own. 
Unsurprisingly, Peurbach’s *Theorica nova* aroused the interest of his contemporaries who immediately grasped its innovative scope. Authors such as Albert of Brudzewo, Francesco Capuano da Manfredonia, Silvestro Mazzolini da Pierio, Wenzel Faber de Budweis, Conrad Tockler, Oronce Finé, Peter Apian, Jacob Milich, Erasmus Reinhold, Oswald Schreckenfuchs, and Christian Wursteisen took up the challenge that Peuerbach launched, and reflected on the new universe he described. The result is a veritable movement of thought, giving rise to and progressively defining a new astronomy. Breaking away from the dominant traditional astronomy bound to an abstractly mathematical approach, this new astronomy launched by the *Theorica nova* created a science of celestial reality where the scientific revolution finds its roots.

The *Theorica* collection has been realized in collaboration with the Centre Jean Pépin (UMR 8230 – CNRS, École Normale Supérieure, Université Paris Sciences Lettres) and [Michela Malpangotto](https://sphaera.mpiwg-berlin.mpg.de/people/).

To visit the collection, click on [DATABASE](http://db.sphaera.mpiwg-berlin.mpg.de/resource/Start) and select the corpus.

[Go to Sacrobosco's sphaera](sacrobosco_sphaera)
