---
layout: default
title: Sacrobosco's De sphaera
date: 2019-11-25T22:25:34.047Z
titleimage: /assets/img/Sphaera.jpg
permalink: /collections/sacrobosco_sphaera
---
{% intro <i>Tractatus de sphaera</i> of Johannes de Sacrobosco %}

During the first half of the thirteenth century in Paris, Johannes de Sacrobosco compiled a new and original tract entitled *Tractatus de sphaera*. The handwritten text, of which several contemporary copies have survived, is an elementary text on geocentric cosmology. It is a simple, late medieval description of the geocentric cosmos in the form of a synthesis of the Aristotelian and Ptolemaic worldviews. The original text also inherited relevant aspects from a series of astronomic works compiled earlier by Arabic scholars, like al-Farghani and Thābit ibn Qurra.

*The Sphere* of Sacrobosco does not show the typical structure of a late medieval commentary: both new and older scholars are cited, but not commented on. The compilation of this treatise is in fact the consequence of an emerging intellectual need for new knowledge; it demonstrates a cultural demand that evolved during the thirteenth century for acquiring knowledge in astronomy and cosmology on a qualitative and descriptive basis, and in the framework of the university network while it was establishing itself.

Although *The Sphere* was not a commentary, it soon began undergoing a process of transformation by means of the practice of commenting, and it became mandatory reading in all European universities up until the seventeenth century—well beyond the publication of Nicolaus Copernicus’ *De revolutionibus orbium coelestium*s in 1543, which proposed a heliocentric structure of the cosmos.

To visit the collection, click on [DATABASE](http://db.sphaera.mpiwg-berlin.mpg.de/resource/Start) and select the corpus.

[Go to Ps.Proclus Sphaera](ps_proclus_sphaera)


