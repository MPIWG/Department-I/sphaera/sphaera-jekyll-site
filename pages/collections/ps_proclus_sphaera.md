---
layout: default
title: Ps.Proclus' De sphaera
date: 2019-11-25T22:25:34.047Z
titleimage: /assets/img/Proclus.jpg
permalink: /collections/ps_proclus_sphaera
---

{% intro <i>De sphaera</i> of Ps.Proclus (𝛱𝛲𝛰𝛫𝛰𝛶 𝛴𝛷𝛢𝛪𝛲𝛢)%}

Pseudo-Proclus’s *Sphaera* is a compilation of chapters 3, 4, 5, and 15 of the ancient work of Geminus of Rhodes (ca. first century BCE) Εἰσαγωγὴ εἰς τὰ φαινόμενα (*Eisagoge eis ta phainomena*) (“Introduction to celestial phenomena”).

The compilation as well as the attribution to Proclus happened in the fifteenth century. The oldest known manuscript showing this compilation is preserved at the Biblioteca Estense of Modena (Gr. 24). Giorgio Valla, who in 1491 compiled the manuscript realized the first Latin translation, expressed the first doubts about the attribution to Proclus.

The work entered the world of the printed books thanks to Thomas Linacre’s Greek edition, printed by the famous Venetian printer and publisher Aldo Manutio within an anthological work in 1499. With more than 70 editions between the *editio princeps* and 1620, the corpus of printings of Ps.Proclus' *Sphaera* represents more than a marginal episode in the history of science. The work became an important textbook used at many European universities. From Venice it was first adopted in the frame of Erasmian and Reformers’s circles, in Basel and Wittenberg respectively. From there, its use spread to other regions, especially Paris.

Ps.Proclus *Sphaera* was used as an enrichment for the usual class in which Sacrobosco’s *Tractatus de sphaera* was read. The elegant ancient Greek and the early modern humanist Latin translation made this work an apt bridge between the study of astronomy and that of ancient classical literature. The shortness and renowned clarity of Ps.Proclus' *Sphaera* allowed this work to become both an introductory reading to ancient works, such as those of Aratus Solensis, Julius Firmicus Maternus, and Marcus Manilius, and at the same time a work to introduce students to typical early modern practical activities related to measurement techniques.

To visit the collection, click on [DATABASE](http://db.sphaera.mpiwg-berlin.mpg.de/resource/Start) and select the corpus.

[Go to Theorica Planetarum](theorica)

