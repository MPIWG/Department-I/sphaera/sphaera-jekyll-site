---
layout: default
title: Home
date: 2020-05-13T14:52:46.568Z
titleimage: /assets/img/sphaera-img.jpg
permalink: /
---
{% intro This project aims at understanding the mechanisms of homogenization and diversification of scientific knowledge from the late medieval epoch until the end of the early modern period. %}

Focused on astronomic and cosmological knowledge, *The Sphere* considers first and foremost knowledge as it was acquired by the largest portion of the society, namely knowledge as it was imparted by the universities. The research therefore turns upside-down the usual approach in history of science and does not focus primarily on singular events such as the publication of Copernicus’s *De revolutionibus orbium coelestium* in 1543 or Galileo’s *Dialogo* in 1632, but rather prioritizes the knowledge that all potential readers of such works actually possessed.

<img align = "left" width="40%" src="assets/img/Worldview-1.jpg" style="margin-right: 0.6em;" alt="Worldview"/>
 
According to actors’s categories, this research investigates the knowledge of *The Sphere*, namely scientific knowledge concerned with geocentric cosmology and codified in most of the period’s widely-used textbooks for teaching spherical cosmology and astronomy.
The shared scientific tradition around *The Sphere* shaped the waxing society of knowledge, one built around the growing number of universities, courts, and all places in which knowledge training took place—such as artist workshops, Abaco schools, and religiously-oriented centers.

After a first phase centered on Johannes de Sacrobosco’s *Tractatus de sphaera*, the project “The Sphere” now comprises two further corpora: one centered on Ps. Proclus’s *Sphaera* and one that includes the most widespread treatise of the genre of the *Theorica planetarum*. This corpus is further subdivided into two collections, one centered on the *Theorica* attributed Gerardus of Cremona and the other on the *Theorica* of Georg von Peurbach.

By focusing on the printed editions, the analysis follows a method of decomposition and re-aggregation of “knowledge atoms”: text-parts, visual elements, and numerical tables. Each kind of knowledge atom builds a synchronic network of semantic relations with other knowledge atoms, and a diachronic one, defined by the atom’s re-occurrence over time. By applying the precepts of the physics of the complex systems such multi-layer networks can be analyzed empirically on a formal level, and on the basis of machine learning techniques, tools for predicting the paths of knowledge evolution are developed. 

<img align = "left" width="44%" src="assets/img/Eclipse_1-1.jpg" alt="Eclipse" style="margin-right: 0.6em;" title="Johannis de Sacro Busto libellus de sphaera, J. Crato, Viterbergae, 1568" />

Taking an extended census of the historical sources, the project aims to reconstruct the larger transformation process—and its mechanisms—at work in the hundreds of historical sources considered, and so doing, to explore the evolutionary path the scientific system traveled between the thirteenth and the seventeenth centuries, as it pivoted around common cosmological knowledge: the backbone of the shared scientific identity of Europe!

<p align="left"> <img/><em align = "left"> <a href = "https://hdl.handle.net/21.11103/sphaera.101112"> Images from: Sacrobosco, Johannes de, and Philipp Melanchthon. Iohannis de Sacro Busto Libellus de Sphaera. Wittenberg: Johann Krafft the Elder, 1568 </a></em> </p>

Please note that as of 01/02/2025, this website is no longer being updated.

{% include cooperations.html %}