---
layout: default
title: Impressum
date: 2019-11-25T22:25:34.047Z
titleimage: /assets/img/sphaera-img.jpg
permalink: /impressum/
---

{% intro Project: “The Sphere. Knowledge System Evolution and the Shared Scientific Identity of Europe” %}


|PI: [Prof. Dr. Matteo Valleriani](https://www.mpiwg-berlin.mpg.de/users/valleriani)| Website: ANIKA MERKLEIN|

<br>

The project “The Sphere. Knowledge System Evolution and the Shared Scientific Identity of Europe” is led at the Max Planck Institute for the History of Science.
For the Imprint, see: [https://www.mpiwg-berlin.mpg.de/page/imprint](https://www.mpiwg-berlin.mpg.de/page/imprint)



