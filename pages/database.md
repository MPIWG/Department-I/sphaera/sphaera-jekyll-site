---
layout: default
title: Database
date: 2019-11-25T22:25:34.047Z
titleimage: /assets/img/databases_banner.jpg
permalink: /database/
---

<br>

<p align="center" class = "intro"> The Sphaera Corpus<i>Tracer</i> database is open to the public and free to access. </p>

[<img width="40%" src="/Department-I/sphaera/sphaera-jekyll-site/assets/img/Rastergrafik.png">](http://db.sphaera.mpiwg-berlin.mpg.de/resource/Start)

{% intro The Book Records %}

Each book of the corpus has its own **record**, similar to common bibliographical datasets. For all books at least one existing copy of the printed edition has been inspected and is related to by locators, leading to library records and/or digital facsimiles.

All book **titles** have been transcribed entirely. **Places** of publication have been adjusted to modern names and can be visualized on a map. **Years** of publication generally have been taken from the book itself. In ambiguous or problematic cases, the year of publication however had to be inferred from circumstantial evidence and is given in square brackets or as timespans.

The **authors** who are credited relate to those names of persons that appear on the title page, neglecting authors whose works may be included in the volume but whose names do not appear on the title page. It has been distinguished between author and translator. For all authors, short and preliminary biographical information is provided where possible, and links to WikiData and other databases can be found.

Reflecting the early modern editorial practice, **printers** and **publishers** have been distinguished wherever indicated as two persons or companies inside the book. Additionally, brief information on **copyright** information and **approval** licenses are given as indicated in the volume itself.

In some cases, the tab “**Annotations**” provides further information on the volume inspected for the record or on further bibliographical information.

Each edition has been “dissected” into text parts. These are taxonomically organized as **original text** or **annotated text** (which can be **commentary** and/or **translation**). Mutual semantic relations among the text parts have been recorded, too. Such semantic relations are: **commentary of**, **translation of**, **fragment of**. Additionally, when one text part has strongly influenced another, the latter is defined as an **adaption of**.
All reoccurrences of the text parts have been recorded.

Together with the text parts, their creators (authors and translators) have also been recorded. **Parts creators** are distinguished from **credited authors**, who are those authors credited on title pages. Generally, the number of parts creators is equal to or higher than the number of **credited authors**.

As for the **edited authors**, short and preliminary biographical information for the **parts creators** is provided where possible, and links to WikiData and other databases can be found.
The language in which the text part has been written is also recorded.

Many editions contain, moreover, numerical and alphanumerical tables. These have been identified by means of the machine learning table classifier [Cor<i><b>Deep</b></i>](https://cordeep.mpiwg-berlin.mpg.de/). Accordingly, a spreadsheet is available for each book that contains the PDF page numbers of pages containing tables. The PDF page numbers refer to the digital copy of the edition linked to or indicated in the database.


