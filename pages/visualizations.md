---
layout: default
title: Visualizations
date: 2019-11-25T22:25:34.047Z
titleimage: /assets/img/Flammarion.jpg
permalink: /visualizations/
---

The following data visualizations are based on data that is available online through external repositories and data that will be make available soon.
Corpus<i>Tracer</i> is an enriched bibliographic metadata collection of the book editions that make up the Sphaera Corpus.
For more information, please see the [publication list](https://sphaera.mpiwg-berlin.mpg.de/publications).
We invite visitors to the database to use the interactive visualizations to help us uncover interesting patterns in the collection. Feel free to contact the project’s PI: [Prof. Dr. Matteo Valleriani](valleriani@mpiwg-berlin.mpg.de).

<br />

<br />

## VIKUS Viewer with numerical tables

About ten-thousand pages displaying numerical tables from the Sphaera corpus. The interactive vizualisation gives an overview when and where the numerical tables were printed and connects each table page with its metadata. 

Credits: Anika Merklein and Hassan el-Hajj

----------------------

<a href = "https://sphaera.mpiwg-berlin.mpg.de/vikusTables/"><img src = "../assets/img/Vikus3.jpeg " alt= "Visualization created by us with the VIKUS Viewer software "></a>

<br />

## Publishing Sacrobosco’s «De sphaera» in Early Modern Europe. Modes of Material and Scientific Exchange, ed. by M. Valleriani and A. Ottone. Cham: Springer Nature, 2021.

The interactive vizualisation maps the research concerned with corpus of 359 different early modern editions of the Tractatus de sphaera of Johannes de Sacrobosco and in reference to the following edited volume: Matteo Valleriani and Andrea Ottone (eds.), Publishing Sacrobosco’s «De sphaera» in Early Modern Europe. Modes of Material and Scientific Exchange, Cham: Springer Nature, 2021. The contributions to the volume mention about 43% of the works of the corpus.

Credits: Florian Kräutli and Hassan el-Hajj

----------------------


<a href = "https://sphaera.mpiwg-berlin.mpg.de/sphaera-printers-volume/"><img src = "../assets/img/book2021_viz.png " alt= "Used sources in Publishing Sacrabosco's De Sphaera in Early Modern Europe"></a>

<br />



## Matteo Valleriani (ed.), De Sphaera of Johannes de Sacrobosco in the Early Modern Period: The Authors of the Commentaries, ed. by M. Valleriani. Cham: Springer Nature, 2020.

The interactive visualisation maps the research concerned with corpus of 359 different early modern editions of the Tractatus de sphaera of Johannes de Sacrobosco and in reference to the following edited volume: Matteo Valleriani (ed.), De Sphaera of Johannes de Sacrobosco in the Early Modern Period: The Authors of the Commentaries, Springer Nature, Cham, 2020. The contributions to the volume mention about 43% of the works of the corpus.

Credits: Florian Kräutli

----------------------


<a href = "https://sphaera.mpiwg-berlin.mpg.de/doi-visualisation-authors-volume/"><img src = "../assets/img/book2019_viz.png " alt= "Used sources in De Sphaera of Johannes in the Eartly Modern Period"></a>

<br />


## Text-Parts Behavior and Production Rate


The 359 editions of the Sphaera Corpus have been dissected into re-occurring text-parts and organized according to a specific semantic taxonomy that reflects the early modern modes of production of scientific knowledge. In addition, this visualization shows the variation of the total number of pages per year, as well as pages with illustrations and those with tables across the period between 1472 and 1650.

The data are linked to the description of the semantic 
taxonomy in the following paper:

<li><span id="RN2503">Zamani, M., Tejedor, A., Vogl, M., Kräutli, F., Valleriani, M., &amp; Kantz, H. (2020). Evolution and Transformation of Early Modern Cosmological Knowledge: A Network Study [Journal Article]. <i>Scientific Reports - Nature</i>. https://doi.org/10.1038/s41598-020-76916-3</span>  <a href="https://doi.org/10.1038/s41598-020-76916-3">Open Access</a></li>



Credits: Florian Kräutli, Daan Lockhorst, and Beate Federau

----------------------


<a href = "https://sphaera.mpiwg-berlin.mpg.de/adoption/"><img src = "../assets/img/sphaera-doi.png " alt= "Sphaera adoption flow created by us with a tool made by Mike Bostock"></a>

<br />




## Scientific Illustrations
Over 20,000 scientific illustrations were extracted from the 359 editions of the Sphaera Corpus. The visualization connects each illustration with the extensive metadata of the editions and their text-parts. The data concerning the scientific illustrations extracted from the corpus are currently being collected as part of the research project

Images and Configurations in Corpora of University Textbooks, which is part of [BIFOLD—Berlin Institute](https://bifold.berlin)
for the Foundations of Learning and Data.

Credits: Florian Kräutli, Daan Lockhorst, and Flavio Gortana

----------------------


<a href = "https://sphaera.mpiwg-berlin.mpg.de/coins/"><img src = "../assets/img/coins.png" alt= ""></a>

<br />
<br />



