---
layout: default
title: Presentations
date: 2019-11-25T22:25:34.047Z
titleimage: /assets/img/Armillarysphere.JPG
permalink: /Presentations/
---

The following presentations are made by us to explain different topics within our body of researchs most of them are made for presenations at conferences, univerisities or working groups. 

For more information about the publications, please see the [publication list](https://sphaera.mpiwg-berlin.mpg.de/publications)

<br />

## Explainable AI for the Sciences: Towards Novel Insights

### Matteo Valleriani at IPAM, UCLA, 2023

----------------------

<iframe width="820" height="500" src="https://www.youtube.com/embed/qvg0kMNxFKE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<br />


<br />

## How Does the Homogenization of Scientific Knowledge Occur?

### Latest Thinking and Matteo Valleriani, 2022

----------------------

<iframe width="820" height="500" src="https://lt.org/embed/5168" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[more information](https://lt.org/publication/how-does-homogenization-scientific-knowledge-occur)

<br />

## Radio Feature with Matteo Valleriani (MPIWG) and Maryam Zamani (MPIPKS) "Mit Algorithmen die europäische Wissenschaftsgeschichte erforschen”
### DLF Kultur - Zeitfragen. July 14, 2021, By Jenny Genzmer (German)


<a href = "https://www.deutschlandfunkkultur.de/digital-history-mit-algorithmen-die-europaeische.976.de.html?dram:article_id=500218"><img src = "../assets/img/sphaera_interview_matteo.jpg" alt= "Sphaera Multiple Layers"></a>

Visualization by means of muxViz. De  Domenico,  Manlio,  Mason  A.  Porter,  Alex  Arenas,  "MuxViz:  A  Tool  for  Multilayer  Analysis  and  Visualization  of  Networks."  Journal  of  Complex Networks 3,no. 2 (2015): 159–76, <a href='https://doi.org/10.1093/comnet/cnu038'>https://doi.org/10.1093/comnet/cnu038</a>

<br />

## The Sphaera Corpus in its Social and Economic Context, by Matteo Valleriani on the 30th of June, 2021. 

----------------------

<iframe width="900" height="720" src="https://www.youtube.com/embed/ilO9QII7gvU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<br />

## TSNE 

----------------------

<iframe width="900" height="720" src="https://www.youtube.com/embed/epwMepJz0m0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


<br />