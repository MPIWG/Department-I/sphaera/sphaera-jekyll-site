---
firstname: Grégoire
lastname: Montavon
image: >-
  /Department-I/sphaera/sphaera-jekyll-site/assets/img/websitephoto_gregoiremontavon.jpg
profile: 'https://www.ml.tu-berlin.de/menue/members/gregoire_montavon'
position: Machine Learning Researcher
---
Affiliation:

Technische Universität Berlin <br/>
