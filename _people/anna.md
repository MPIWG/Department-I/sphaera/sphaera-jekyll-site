---
firstname: Anna 
lastname: I. Siebold
image: /Department-I/sphaera/sphaera-jekyll-site/assets/img/AnnaSiebold_0.jpg
profile: 'https://www.mpiwg-berlin.mpg.de/users/asiebold'
position: PhD Candidate in History
---
Affiliation:

Carl von Ossietzky University of Oldenburg<br/>
Max Planck Institute for the History of Science