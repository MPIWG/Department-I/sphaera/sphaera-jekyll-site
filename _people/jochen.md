---
firstname: Jochen
lastname: Büttner
image: /Department-I/sphaera/sphaera-jekyll-site/assets/img/20170530-buttner_jochen_82718f150dpi.jpg
profile: 'https://www.shh.mpg.de/person/134197/2268875'
position: Historian, Machine Learning Developer
order: 30
---

Affiliation:

Max Planck Institute of Geoanthropology, Jena