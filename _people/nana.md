---
firstname: Nana
lastname: Citron
image: >-
  /Department-I/sphaera/sphaera-jekyll-site/assets/img/websitephoto_nanacitron.jpeg
profile: ' '
position: PhD Candidate in History
---

Affiliation:

University of Oslo <br/>
Max Planck Institute for the History of Science
