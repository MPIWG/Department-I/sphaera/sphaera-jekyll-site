---
firstname: Noga
lastname: Shlomi
image: >-
  /Department-I/sphaera/sphaera-jekyll-site/assets/img/websitephoto_nogashlomi.jpg
profile: 'https://www.mpiwg-berlin.mpg.de/users/nshlomi'
position: PhD Candidate in History
---
Affiliation:

Cohn Institute for the History and Philosophy of Science and Ideas, Tel Aviv University<br/>
Max Planck Institute for the History of Science
