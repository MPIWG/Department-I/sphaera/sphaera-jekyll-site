---
firstname: Matteo
lastname: Valleriani
image: /Department-I/sphaera/sphaera-jekyll-site/assets/img/sphere_prudentia_minerbi.png
profile: https://www.mpiwg-berlin.mpg.de/users/valleriani
position: Research Group Leader / Principal Investigator
order: 10
---

Affiliation:

Max Planck Institute for the History of Science <br/>
Berlin Institute for the Foundations of Learning and Data (BIFOLD) <br/>
Technische Universität Berlin <br/>
Tel Aviv University