---
firstname: Stephanie
lastname: Hood
image: /Department-I/sphaera/sphaera-jekyll-site/assets/img/mpiwg-20170530-1451_hood_stephanie150dpi-srgbcsetform.de_ 2.jpeg
profile: https://www.mpiwg-berlin.mpg.de/en/users/shood
position: Editing & Communication
order: 60

---

Affiliation:

Max Planck Institute for the History of Science