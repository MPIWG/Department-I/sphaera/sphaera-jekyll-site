---
firstname: Esther
lastname: Chen
image: /Department-I/sphaera/sphaera-jekyll-site/assets/img/20170515-chen_esther_0545_2150dpi-srgbcsetform.de_.edited_0.jpg
profile: 'https://www.mpiwg-berlin.mpg.de/users/echen'
position: Head of the Library
order: 40
---

Affiliation:

Max Planck Institute for the History of Science

