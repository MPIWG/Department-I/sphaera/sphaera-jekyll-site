---
firstname: Michela
lastname: Malpangotto
image: /Department-I/sphaera/sphaera-jekyll-site/assets/img/michela.png
profile: https://umr8230.cnrs.fr/membres/michela-malpangotto/
position: Directrice de recherche / Principal Investigator
order: 
---

Affiliation:

Centre National de la Recherche Scientifique <br/>