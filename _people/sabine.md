---
firstname: Sabine
lastname: Bertram
image: /Department-I/sphaera/sphaera-jekyll-site/assets/img/Sabi.jpeg
profile: 'https://www.mpiwg-berlin.mpg.de/users/bertram'
position: Head of Collection
order: 70
---

Affiliation:

Max Planck Institute for the History of Science
 
